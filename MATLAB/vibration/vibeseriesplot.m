fakesum
realsum
%%
micsens=0.000956;                        %sensitivity of microphone v/pa
preamp_gain=100;                       %Gain on nexus amplifier
mix_gain_db=30;                             %gain on soundcard
mix_gain=10^(mix_gain_db/10);
sc_gain_db=6;
sc_gain=10^(sc_gain_db/10);
totalsens1=micsens*preamp_gain*mix_gain*sc_gain; %total sensitivity of 

micsens=0.000956;                        %sensitivity of microphone v/pa
preamp_gain=100;                       %Gain on nexus amplifier
mix_gain_db=30;                             %gain on soundcard
mix_gain=10^(mix_gain_db/10);
sc_gain_db=3;
sc_gain=10^(sc_gain_db/10);
totalsens2=micsens*preamp_gain*mix_gain*sc_gain; %total sensitivity of 

%%
width = 120;
height = 90;

viplot = figure();
semilogy(timeseries1(:,1)-2,timeseries1(:,2)./(totalsens1*vref),... %'real' values
    timeseries2(:,1)-2, timeseries2(:,2)./(totalsens2*vref)); %'fake' values

legend('Measured from Dyson V8','Measured from VCPD');
xlabel('Time (t)');
ylabel('Vibration Strength, RMS m/s^2')
ylim([1 100]);

set(viplot,...
    'units','centimeters',...
    'Position',[0 0 width/10 height/10])

set(gca,...
    'units','centimeters',...
    'Position',[2 2 (width/10 - 3) (height/10 - 3)], ...
    'FontUnits','points',...
    'FontWeight','normal',...
    'FontSize',11,... 
    'FontName','Times')

print('VibeCompare','-depsc2')