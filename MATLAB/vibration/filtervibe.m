function [ rmsums1 ] = filtervibe( wavefile )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[a1,Fs]=audioread(wavefile);
samplehalfwidth = 10000;
stepsize = 100;
t = [];
rmsums1 = [];
rmsums2 = [];


BW = '1/3 octave';
N = 8;
F0 = 1000;
oneThirdOctaveFilter = octaveFilter('FilterOrder', N, ...
    'CenterFrequency', F0, 'Bandwidth', BW, 'SampleRate', Fs);
F0 = getANSICenterFrequencies(oneThirdOctaveFilter);
F0(F0<6) = [];
F0(F0>1300) = [];
Wf = [
    0.727,
    0.873,
    0.951,
    0.958,
    0.896,
    0.782,
    0.647,
    0.519,  
    0.411,
    0.324,
    0.256,
    0.202,
    0.160,
    0.127,
    0.101,
    0.0799,
    0.0634,
    0.0503,
    0.0398,
    0.0314,
    0.0245,
    0.0186,
    0.0135,
    0.00894];
Nfc = length(F0);
for i=1:Nfc
    oneThirdOctaveFilterBank{i} = octaveFilter('FilterOrder', N, ...
        'CenterFrequency', F0(i), 'Bandwidth', BW, 'SampleRate', Fs); %#ok
end

progress = waitbar(0,'calcumalating');
for n=1:stepsize:(length(a1) - samplehalfwidth)
    t = [t n];
    waitbar(n/(length(a1) - samplehalfwidth));
    rmsums1 = [rmsums1 ; 0];
    for i=1:Nfc
        throcto1 = oneThirdOctaveFilterBank{i};
        filtered1 = throcto1(a1((n:n+samplehalfwidth)));
        rmsums1(length(t)) = rmsums1(length(t)) + Wf(i) * rms(filtered1);
    end
end
close(progress);

%% 
rmsums1 = smooth(rmsums1, 10);