t = linspace(0,5,100);

x = [0.01 0.03 0.3];
v = [0 100 0];
y = interp1(x,v,exp(-1-t),'linear',0);

nx = [0.01 0.02 0.03 0.1 0.3];
nv = [0 50 100 50 0];
ny = interp1(nx,nv,exp(-1-t),'linear',0);


plot(t,y,'r-',exp(-1-x),v,'r.',t,ny,'b-',exp(-1-nx),nv,'b.');
