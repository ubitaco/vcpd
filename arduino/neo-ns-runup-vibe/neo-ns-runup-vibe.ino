#include "filters.h"

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

#include "Adafruit_DRV2605.h"

#define MAX9744_I2CADDR 0x4B

// GUItool: begin automatically generated code
AudioSynthWaveformSine   sine0;          //xy=846,422
AudioSynthWaveformSine   sine1;          //xy=847,457
AudioSynthWaveformSine   sine2;          //xy=849,493
AudioSynthWaveformSine   sine3;          //xy=849,527
AudioSynthNoiseWhite     noise1;         //xy=853,247
AudioFilterBiquad        noisefilter1;   //xy=985,249
AudioMixer4              toneMixer;      //xy=1002,480
AudioFilterBiquad        noisefilter2;   //xy=1027,307
AudioMixer4              NTMixer;        //xy=1195,328
AudioFilterBiquad        TRfilter1;      //xy=1340,311
AudioFilterBiquad        TRfilter2;      //xy=1353,376
AudioFilterBiquad        TRfilter3;        //xy=1371,437
AudioFilterBiquad        Correction1;        //xy=1392,499
AudioFilterBiquad        Correction2;        //xy=1409,557
AudioFilterBiquad        Notch1;        //xy=1439,615
AudioMixer4              FinalGain;
AudioOutputI2S           i2s1;           //xy=1688,548
AudioConnection          patchCord1(sine0, 0, toneMixer, 0);
AudioConnection          patchCord2(sine1, 0, toneMixer, 1);
AudioConnection          patchCord3(sine2, 0, toneMixer, 2);
AudioConnection          patchCord4(sine3, 0, toneMixer, 3);
AudioConnection          patchCord5(noise1, noisefilter1);
AudioConnection          patchCord6(noisefilter1, noisefilter2);
AudioConnection          patchCord7(toneMixer, 0, NTMixer, 1);
AudioConnection          patchCord8(noisefilter2, TRfilter1);
AudioConnection          patchCord9(NTMixer, 0, FinalGain, 0);
AudioConnection          patchCord10(TRfilter1, TRfilter2);
AudioConnection          patchCord11(TRfilter2, TRfilter3);
AudioConnection          patchCord12(TRfilter3, Correction1);
AudioConnection          patchCord13(Correction1, Correction2);
AudioConnection          patchCord14(Correction2, Notch1);
AudioConnection          patchCord15(TRfilter3, 0, NTMixer, 0);
AudioConnection          patchCord16(FinalGain, 0, i2s1, 0);
AudioConnection          patchCord17(FinalGain, 0, i2s1, 1);
AudioControlSGTL5000     sgtl5000_1;     //xy=751,196
// GUItool: end automatically generated code

// Declare vibe driver
Adafruit_DRV2605 vibe;


int tones[4] = {11320, 6476, 1632, 21320};
float toneamps[4] = {0.4, 0.9, 0.3, 0.008};
float spin = 0.0;
float target = 0.0;
short tspin = 10;
short timeconstant_spinup = 250;
short timeconstant_spindown = 1000;
float spinup_step_change;
float spindown_step_change;
float lowspindown_step_change;
float highspindown_step_change;
float namp = 0.1;
float newnamp = 0.0;
float vibexp = 0.03; //value of spin for max vibration
float vibexc = 0.13; //highest value of spin for non-zero vibration
float vibeyp = 127; //strength of the strongest vibration. max possible is 127 I think.
float tonexp = 0.05;
float tonexc = 0.2;
float toneyp = 0.3;


void updatetones() {
  sine0.amplitude(pow(spin,2) * toneamps[0]);
  sine1.amplitude(pow(spin,2) * toneamps[1]);
  sine2.amplitude(pow(spin,2) * toneamps[2]);
  sine3.amplitude(pow(spin,2) * toneamps[3]);

  sine0.frequency(spin * tones[0]);
  sine1.frequency(spin * tones[1]);
  sine2.frequency(spin * tones[2]);
  sine3.frequency(spin * tones[3]);

  if(spin < tonexc) {
      sine1.amplitude(((tonexc-spin)/(tonexc-tonexp)) * toneyp * toneamps[1]
                        + pow(spin * toneamps[1],2));
  }
  if(spin < 0.05) {
      sine1.amplitude((spin/tonexp) * toneyp * toneamps[1]
                        + pow(spin * toneamps[1],2));
  }
}

void updatevibe() {
    if (spin > vibexc) {
        vibe.setRealtimeValue(0);
    }
    else if (spin > vibexp) {
        vibe.setRealtimeValue(
            (int)((vibeyp / (vibexc - vibexp)) * (vibexc - spin))
        );
    }
    else if (spin > 0.01) {
        vibe.setRealtimeValue((int)(spin * (vibeyp / vibexp)));
    }
    else {
        vibe.setRealtimeValue(0);
    }
}

float updatespin(float oldspin, float targetspin, short tstep) {
  if (targetspin > oldspin) {
    return (spinup_step_change * oldspin) + ((1-spinup_step_change) * target);
  }
  else if (oldspin < 0.1 && oldspin > 0.02) {
    return (lowspindown_step_change * oldspin)
              + ((1-(lowspindown_step_change)) * target);
  }
  else if (oldspin > 0.3) {
    return (highspindown_step_change * oldspin)
              + ((1-(highspindown_step_change)) * target);
  }
  else {
    return (spindown_step_change * oldspin) + ((1-spindown_step_change) * target);
}
}

boolean setvolume(int8_t v) {
  // cant be higher than 63 or lower than 0
  if (v > 63) v = 63;
  if (v < 0) v = 0;

  Serial.print("Setting volume to ");
  Serial.println(v);
  Wire.beginTransmission(MAX9744_I2CADDR);
  Wire.write(v);
  if (Wire.endTransmission() == 0)
    return true;
  else
    return false;
}

void setup() {
  //calculated variables:
  spinup_step_change = exp(-1 * tspin * 2.9957 / timeconstant_spinup);
  spindown_step_change = exp(-1 * tspin * 2.9957 / timeconstant_spindown);
  lowspindown_step_change = exp(-1 * tspin * 2.9957 / 1000);
  highspindown_step_change = exp(-1 * tspin * 2.9957 / 1000);

  // put your setup code here, to run once:
  Serial.begin(9600);
  AudioMemory(8);
  sgtl5000_1.enable();
  sgtl5000_1.volume(1.0);
  pinMode(21, INPUT_PULLUP);
  setvolume(63);

  //initialize vibe driver
  vibe.begin();
  vibe.setMode(DRV2605_MODE_REALTIME);

  // assign values to sine objects:
  updatetones();

  noisefilter1.setCoefficients(0,highNF10);
  noisefilter1.setCoefficients(1,highNF11);
  noisefilter1.setCoefficients(2,highNF12);
  noisefilter1.setCoefficients(3,highNF13);
  noisefilter2.setCoefficients(0,highNF20);
  noisefilter2.setCoefficients(1,highNF21);
  noisefilter2.setCoefficients(2,highNF22);
  noisefilter2.setCoefficients(3,highNF23);

  TRfilter1.setCoefficients(0,TRF10);
  TRfilter1.setCoefficients(1,TRF11);
  TRfilter1.setCoefficients(2,TRF12);
  TRfilter1.setCoefficients(3,TRF13);
  TRfilter2.setCoefficients(0,TRF20);
  TRfilter2.setCoefficients(1,TRF21);
  TRfilter2.setCoefficients(2,TRF22);
  TRfilter2.setCoefficients(3,TRF23);
  TRfilter3.setCoefficients(0,TRF30);
  TRfilter3.setCoefficients(1,TRF31);
  TRfilter3.setCoefficients(2,TRF32);
  TRfilter3.setCoefficients(3,TRF33);

  Correction1.setCoefficients(0,Cor10);
  Correction1.setCoefficients(1,Cor11);
  Correction1.setCoefficients(2,Cor12);
  Correction1.setCoefficients(3,Cor13);
  Correction2.setCoefficients(0,Cor20);
  Correction2.setCoefficients(1,Cor21);
  Correction2.setCoefficients(2,Cor22);
  Correction2.setCoefficients(3,Cor23);

  // Notch1.setNotch(0,16170, 100);

  toneMixer.gain(0,0.1);
  toneMixer.gain(1,0.1);
  toneMixer.gain(2,0.1);
  toneMixer.gain(3,0.1);

  NTMixer.gain(0,2);  // Noise Gain
  NTMixer.gain(1,0.5); // Tone Gain

  FinalGain.gain(0,1.0);

}

void loop() {
  // put your main code here, to run repeatedly:
  AudioNoInterrupts();

  newnamp = namp + (random(2) -1) * 0.01;
  if (newnamp < 0.08 && newnamp > 0.12) {
    namp = newnamp;
    noise1.amplitude(spin * namp);
}
  // if (digitalRead(21) == LOW) {
  //   target = 1.0;
  //   Serial.println("21 is LOW");
  // }
  // else {
  //   target = 0.0;
  //   Serial.println("21 is HIGH");
  // }

  spin = updatespin(spin, target, tspin);
  updatetones();
  updatevibe();
  noise1.amplitude(pow(spin, 4) * namp);

  target = (float)(((millis() / 10000)+1) % 2);

  AudioInterrupts();
  delay(tspin);

}
