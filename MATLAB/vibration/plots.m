% %% Plots
% lmax=max([max(abs(Pxx1)),max(abs(Pxx2)),max(abs(Pxx3)),max(abs(Pxx4)),max(abs(Pxx5)),max(abs(Pxx6)),max(abs(Pxx7)),max(abs(Pxx8))]);
% lmin=min([min(abs(Pxx1)),min(abs(Pxx2)),min(abs(Pxx3)),min(abs(Pxx4)),min(abs(Pxx5)),min(abs(Pxx6)),min(abs(Pxx7)),min(abs(Pxx8))]);
% %%
% figure()
% semilogx(F,10*log10(m1))
% hold on
% semilogx(F0,10*log10(abs(H8))) 
% semilogx(F0,10*log10(abs(H16)))
% semilogx(F0,10*log10(abs(H24)))
% semilogx(F,10*log10(abs(Pxx5))) 
% semilogx(F,10*log10(abs(Pxx6)))
% semilogx(F,10*log10(abs(Pxx7)))
% semilogx(F,10*log10(abs(Pxx8)))

%%
xlim([50 22050])
%ylim([-min(abs(TR)) max(abs(TR))])
set(gca,...
    'units','normalized',...
    'YTick',-100:5:100,...
    'Position',[.15 .2 .75 .7],...
    'FontUnits','points',...
    'FontWeight','normal',...
    'FontSize',10,... 
    'FontName','Times')


% zlabel({'Magnitude (dB)'},...
%     'FontUnits','points',...
%     'FontWeight','normal',...
%     'interpreter','latex',...
%     'FontSize',11,...
%     'FontName','Times')
ylabel({'Magnitude (dB)'},...
    'FontUnits','points',...
    'FontWeight','normal',...
    'interpreter','latex',...
    'FontSize',11,...
    'FontName','Times')
xlabel('Frequency (Hz)',...
    'FontUnits','points',...
    'FontWeight','normal',...
    'interpreter','latex',...
    'FontSize',11,...
    'FontName','Times')

% legend({'Microphone position 1','Microphone position 3','Microphone position 5','Microphone position 7'},...
%      'FontUnits','points',...
%      'interpreter','latex',...
%      'FontSize',7,...
%      'FontName','Times',...
%      'Location','South')
% legend({'fundamental frequency','3rd harmonic','4th harmonic','7th harmonic','9th harmonic'},...
%      'FontUnits','points',...
%      'interpreter','latex',...
%      'FontSize',7,...
%      'FontName','Times',...
%      'Location','NorthWest')


title('Spectrogram for high power VCPD run-down',...
    'FontUnits','points',...
    'FontWeight','normal',...
    'FontSize',11,...
    'FontName','Times')


%%

