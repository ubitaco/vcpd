[a1,Fs]=audioread('realvibe.wav');
[a2,Fs2]=audioread('fakevibe2.wav');
samplehalfwidth = 1000;
stepsize = 100;
t = [];
rmsums1 = [];
rmsums2 = [];


BW = '1/3 octave';
N = 8;
F0 = 1000;
oneThirdOctaveFilter = octaveFilter('FilterOrder', N, ...
    'CenterFrequency', F0, 'Bandwidth', BW, 'SampleRate', Fs);
F0 = getANSICenterFrequencies(oneThirdOctaveFilter);
F0(F0<20) = [];
F0(F0>1300) = [];
Wf = [0.647,
    0.519,
    0.411,
    0.324,
    0.256,
    0.202,
    0.160,
    0.127,
    0.101,
    0.0799,
    0.0634,
    0.0503,
    0.0398,
    0.0314,
    0.0245,
    0.0186,
    0.0135,
    0.00894];
Nfc = length(F0);
for i=1:Nfc
    oneThirdOctaveFilterBank{i} = octaveFilter('FilterOrder', N, ...
        'CenterFrequency', F0(i), 'Bandwidth', BW, 'SampleRate', Fs); %#ok
end

for i=1:Nfc
    twoThirdOctaveFilterBank{i} = octaveFilter('FilterOrder', N, ...
        'CenterFrequency', F0(i), 'Bandwidth', BW, 'SampleRate', Fs); %#ok
end

progress = waitbar(0,'calcumalating');
for n=1:stepsize:(min(length(a1),length(a2)) - samplehalfwidth)
    t = [t n];
    waitbar(n/(min(length(a1),length(a2)) - samplehalfwidth));
    rmsums1 = [rmsums1 0];
    rmsums2 = [rmsums2 0];
    for i=1:Nfc
        throcto1 = oneThirdOctaveFilterBank{i};
        filtered1 = throcto1(a1((n:n+samplehalfwidth)));
        rmsums1(length(t)) = rmsums1(length(t)) + Wf(i) * rms(filtered1);
        
        throcto2 = twoThirdOctaveFilterBank{i};
        filtered2 = throcto2(a2((n:n+samplehalfwidth)));
        rmsums2(length(t)) = rmsums2(length(t)) + Wf(i) * rms(filtered2);
    end
end
close(progress);

%% 

plot((t / Fs)-5,rmsums1,(t / Fs)-5,rmsums2);
legend('Realvibe','Fakevibe');
xlabel('Time (t)');
ylabel('Vibration Strength, RMS m/s^2 (BS-10819')
print('VibeCompare','-dpng')
