#include "filters.h"

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// GUItool: begin automatically generated code
AudioSynthWaveformSine   sine0;          //xy=846,422
AudioSynthWaveformSine   sine1;          //xy=847,457
AudioSynthWaveformSine   sine2;          //xy=849,493
AudioSynthWaveformSine   sine3;          //xy=849,527
AudioSynthNoiseWhite     noise1;         //xy=853,247
AudioFilterBiquad        noisefilter1;   //xy=985,249
AudioMixer4              toneMixer;      //xy=1002,480
AudioFilterBiquad        noisefilter2;   //xy=1027,307
AudioMixer4              NTMixer;        //xy=1195,328
AudioFilterBiquad        TRfilter1;      //xy=1340,311
AudioFilterBiquad        TRfilter2;      //xy=1353,376
AudioFilterBiquad        TRfilter3;        //xy=1371,437
AudioFilterBiquad        Correction1;        //xy=1392,499
AudioFilterBiquad        Correction2;        //xy=1409,557
AudioFilterBiquad        Notch1;        //xy=1439,615
AudioMixer4              FinalGain;
AudioOutputI2S           i2s1;           //xy=1688,548
AudioConnection          patchCord1(sine0, 0, toneMixer, 0);
AudioConnection          patchCord2(sine1, 0, toneMixer, 1);
AudioConnection          patchCord3(sine2, 0, toneMixer, 2);
AudioConnection          patchCord4(sine3, 0, toneMixer, 3);
AudioConnection          patchCord5(noise1, noisefilter1);
AudioConnection          patchCord6(noisefilter1, noisefilter2);
AudioConnection          patchCord7(toneMixer, 0, NTMixer, 1);
AudioConnection          patchCord8(noisefilter2, 0, NTMixer, 0);
AudioConnection          patchCord9(NTMixer, TRfilter1);
AudioConnection          patchCord10(TRfilter1, TRfilter2);
AudioConnection          patchCord11(TRfilter2, TRfilter3);
AudioConnection          patchCord12(TRfilter3, Correction1);
AudioConnection          patchCord13(Correction1, Correction2);
AudioConnection          patchCord14(Correction2, Notch1);
AudioConnection          patchCord15(Notch1, 0, FinalGain, 0);
AudioConnection          patchCord16(FinalGain, 0, i2s1, 0);
AudioConnection          patchCord17(FinalGain, 0, i2s1, 1);
AudioControlSGTL5000     sgtl5000_1;     //xy=751,196
// GUItool: end automatically generated code


int tones[4] = {11320, 6476, 9604, 21320};
float toneamps[4] = {0.4, 0.9, 0.01, 0.008};
float spin = 0.0;
float target = 0.0;
short tspin = 10;
short timeconstant_spinup = 250;
short timeconstant_spindown = 1000;
float spinup_step_change;
float spindown_step_change;
float lowspindown_step_change;
float highspindown_step_change;
float namp = 1.0;
float newnamp = 0.0;


void updatetones() {
  sine0.amplitude(spin * toneamps[0]);
  sine1.amplitude(spin * toneamps[1]);
  sine2.amplitude(spin * toneamps[2]);
  sine3.amplitude(spin * toneamps[3]);

  sine0.frequency(spin * tones[0]);
  sine1.frequency(spin * tones[1]);
  sine2.frequency(spin * tones[2]);
  sine3.frequency(spin * tones[3]);

  if(spin < 0.1) {
      sine1.amplitude((((-0.2 / 0.05) * (spin - 0.1))+0.1)* toneamps[1]);
  }
  if(spin < 0.05) {
      sine1.amplitude(spin * toneamps[1] * (0.3 / 0.05));
  }
}

float updatespin(float oldspin, float targetspin, short tstep) {
  if (targetspin > oldspin) {
    return (spinup_step_change * oldspin) + ((1-spinup_step_change) * target);
  }
  else if (oldspin < 0.1 && oldspin > 0.02) {
    return (lowspindown_step_change * oldspin)
              + ((1-(lowspindown_step_change)) * target);
  }
  else if (oldspin > 0.3) {
    return (highspindown_step_change * oldspin)
              + ((1-(highspindown_step_change)) * target);
  }
  else {
    return (spindown_step_change * oldspin) + ((1-spindown_step_change) * target);
  }
}


void setup() {
  //calculated variables:
  spinup_step_change = exp(-1 * tspin * 2.9957 / timeconstant_spinup);
  spindown_step_change = exp(-1 * tspin * 2.9957 / timeconstant_spindown);
  lowspindown_step_change = exp(-1 * tspin * 2.9957 / 3000);
  highspindown_step_change = exp(-1 * tspin * 2.9957 / 300);

  // put your setup code here, to run once:
  Serial.begin(9600);
  AudioMemory(8);
  sgtl5000_1.enable();
  sgtl5000_1.volume(1.0);
  pinMode(21, INPUT_PULLUP);

  // assign values to sine objects:
  updatetones();

  noisefilter1.setCoefficients(0,highNF10);
  noisefilter1.setCoefficients(1,highNF11);
  noisefilter1.setCoefficients(2,highNF12);
  noisefilter1.setCoefficients(3,highNF13);
  noisefilter2.setCoefficients(0,highNF20);
  noisefilter2.setCoefficients(1,highNF21);
  noisefilter2.setCoefficients(2,highNF22);
  noisefilter2.setCoefficients(3,highNF23);

  TRfilter1.setCoefficients(0,TRF10);
  TRfilter1.setCoefficients(1,TRF11);
  TRfilter1.setCoefficients(2,TRF12);
  TRfilter1.setCoefficients(3,TRF13);
  TRfilter2.setCoefficients(0,TRF20);
  TRfilter2.setCoefficients(1,TRF21);
  TRfilter2.setCoefficients(2,TRF22);
  TRfilter2.setCoefficients(3,TRF23);
  TRfilter3.setCoefficients(0,TRF30);
  TRfilter3.setCoefficients(1,TRF31);
  TRfilter3.setCoefficients(2,TRF32);
  TRfilter3.setCoefficients(3,TRF33);

  Correction1.setCoefficients(0,Cor10);
  Correction1.setCoefficients(1,Cor11);
  Correction1.setCoefficients(2,Cor12);
  Correction1.setCoefficients(3,Cor13);
  Correction2.setCoefficients(0,Cor20);
  Correction2.setCoefficients(1,Cor21);
  Correction2.setCoefficients(2,Cor22);
  Correction2.setCoefficients(3,Cor23);

  Notch1.setNotch(0,16170, 100);

  toneMixer.gain(0,0.1);
  toneMixer.gain(1,0.1);
  toneMixer.gain(2,0.1);
  toneMixer.gain(3,0.1);

  NTMixer.gain(0,0.1);  // Noise Gain
  NTMixer.gain(1,4); // Tone Gain

  FinalGain.gain(0,30.0);

}

void loop() {
  // put your main code here, to run repeatedly:
  AudioNoInterrupts();
/**
  newnamp = namp + (random(2) -1) * 0.1;
  if (newnamp < 0.4 && newnamp > 0.6) {
    namp = newnamp;
    noise1.amplitude(spin * namp);
}**/

  if (digitalRead(21) == LOW) {
    target = 1.0;
    Serial.println("21 is LOW");
  }
  else {
    target = 0.0;
    Serial.println("21 is HIGH");
  }

  spin = updatespin(spin, target, tspin);
  updatetones();
  noise1.amplitude(pow(spin * namp, 4));

  // target = (float)(((millis() / 10000)+1) % 2);

  AudioInterrupts();
  Serial.print("all=");
  Serial.println(AudioProcessorUsage());
  delay(tspin);

}
