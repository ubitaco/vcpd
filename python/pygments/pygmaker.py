from pygments import highlight
from pygments.lexers.c_cpp import CppLexer
from pygments.formatters import LatexFormatter

code = """void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:

}
"""
print(highlight(code, CppLexer(), LatexFormatter()))
