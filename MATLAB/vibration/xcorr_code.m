a=[length(y1),length(y2),length(y3),length(y4),length(y5)];
ma=max(a);

%% Generate Time Vector
t=linspace(0,ma/fs,ma);
%% Plots for inspection
n=y3;
% figure()
% plot(n); %t(1:length(n))
%% Roughly seperating 'Run Down' for 5 recordings
% a=y1(467300:end);       al=length(a);
% b2=y2(507400:end);      b2l=length(b2);
% b3=y3(529000:end);      b3l=length(b3);  %LOW 1
% b4=y4(566000:end);      b4l=length(b4);
% b5=y5(524800:end);      b5l=length(b5);

a=y1;       al=length(a);
b2=y2;      b2l=length(b2);
b3=y3;      b3l=length(b3);   %HIGH 1
b4=y4;      b4l=length(b4);
b5=y5;      b5l=length(b5);

rdl=[al,b2l,b3l,b4l,b5l]; %run down length
[rdl_max,idmax]=max(rdl);


A=padarray(a,[(rdl_max-al) 0],'post');
B2=padarray(b2,[(rdl_max-b2l) 0],'post');
B3=padarray(b3,[(rdl_max-b3l) 0],'post');
B4=padarray(b4,[(rdl_max-b4l) 0],'post');
B5=padarray(b5,[(rdl_max-b5l) 0],'post');

%%
c2=xcorr(A,B2);
c3=xcorr(A,B3);
c4=xcorr(A,B4);
c5=xcorr(A,B5);

[corr2, id2] = max(c2);
[corr3, id3] = max(c3);
[corr4, id4] = max(c4);
[corr5, id5] = max(c5);

mid=length(c2)/2;
%%
shift(1)=round(id2-mid);
shift(2)=round(id3-mid);
shift(3)=round(id4-mid);
shift(4)=round(id5-mid);
msh=max(shift);
shift=shift-msh;
%%
cB2=circshift(B2,[shift(1),0]);
cB3=circshift(B3,[shift(2),0]);
cB4=circshift(B4,[shift(3),0]);
cB5=circshift(B5,[shift(4),0]);
%%
avgA=A(1:al);
avgB2=cB2(1:al);
avgB3=cB3(1:al);
avgB4=cB4(1:al);
avgB5=cB5(1:al);

avg=(avgA+avgB2+avgB3+avgB4+avgB5)/5;
%%
window=hanning(2000);
overlap=0;

%%
micsens=0.000956;                        %sensitivity of microphone v/pa
preamp_gain=100;                       %Gain on nexus amplifier
mix_gain_db=30;                             %gain on soundcard
mix_gain=10^(mix_gain_db/10);
sc_gain_db=6;
sc_gain=10^(sc_gain_db/10);
totalsens=micsens*preamp_gain*mix_gain*sc_gain; %total sensitivity of 

%% SPECTROGRAM
thresh=10^(50/10);
vref=10^(-6);
% spectrogram((avg./(totalsens*vref)),window,overlap,fs,'yaxis','MinThreshold',0);
%%
% figure(1)
% title('recording 2 correlated');
% xlabel('Time (s)');
% ylabel('voltage (V)')
% figure(2)
% subplot(2,2,1);
% plot(t(1:length(cB2)),cB2)
% title('recording 2 correlated');
% xlabel('Time (s)');
% ylabel('voltage (V)')
% subplot(2,2,2);
% plot(t(1:length(cB2)),cB3)
% title('recording 3 correlated');
% xlabel('Time (s)');
% ylabel('voltage (V)')
% subplot(2,2,3);
% plot(t(1:length(cB2)),cB4)
% title('recording 4 correlated');
% xlabel('Time (s)');
% ylabel('voltage (V)')
% subplot(2,2,4);
% plot(t(1:length(cB2)),cB5)
% title('recording 5 correlated ');
% xlabel('Time (s)');
% ylabel('voltage (V)')
% %%
% subplot(2,2,1);
% plot(c2)
% title('recording 2 correlation');
% xlabel('samples (n)');
% ylabel('cross-correlation coefficient')
% subplot(2,2,2);
% plot(c3)
% title('recording 3 correlation');
% xlabel('samples (n)');
% ylabel('cross-correlation coefficient')
% subplot(2,2,3);
% plot(c4)
% title('recording 4 correlation');
% xlabel('samples (n)');
% ylabel('cross-correlation coefficient')
% subplot(2,2,4);
% plot(c5)
% title('recording 5 correlation ');
% xlabel('samples (n)');
% ylabel('cross-correlation coefficient')
% %%
% figure()
% plot(t(1:al),avg); %t(1:length(cB2))
% title('Run Down average');
% xlabel('Time (s)');
% ylabel('voltage (V)')