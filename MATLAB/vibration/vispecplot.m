%%
width = 400;
height = 300;


%% fake part
fakeloader
xcorr_code

samwidth = 1000;
window=hanning(samwidth);
overlap = 0;%floor(samwidth * 0.5);

vispec = figure();
spectrogram(1.4*(avg./(totalsens*vref)),window,overlap,linspace(2,1400,2^8),fs,'yaxis','MinThreshold',-40);

set(gcf,...
    'units','centimeters',...
    'Position',[0 0 width/10 height/10])

set(gca,...
    'units','centimeters',...
    'Position',[2 2 (width/10 - 5) (height/10 - 3)], ...
    'FontUnits','points',...
    'FontWeight','normal',...
    'FontSize',11,...
    'FontName','Times');

print -depsc2 figs/VCPDrdspectro.eps

%% real part
realloader
xcorr_code
vispec = figure();
window=hanning(samwidth);
overlap = floor(samwidth * 0.9);

vispec = figure();
spectrogram(1.4*(avg./(totalsens*vref)),window,overlap,linspace(2,1400,2^8),fs,'yaxis','MinThreshold',-40);

set(vispec,...
    'units','centimeters',...
    'Position',[0 0 width/10 height/10])

set(gca,...
    'units','centimeters',...
    'Position',[2 2 (width/10 - 5) (height/10 - 3)], ...
    'FontUnits','points',...
    'FontWeight','normal',...
    'FontSize',11)

print -depsc2 figs/DV8rdspectro.eps
