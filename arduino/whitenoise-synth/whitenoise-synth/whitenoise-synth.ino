#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// GUItool: begin automatically generated code
AudioSynthNoiseWhite     noise1;         //xy=480,177
AudioOutputI2S           i2s1;           //xy=667,177
AudioConnection          patchCord1(noise1, 0, i2s1, 0);
AudioConnection          patchCord2(noise1, 0, i2s1, 1);
AudioControlSGTL5000     sgtl5000_1;     //xy=585,279
// GUItool: end automatically generated code

void setup() {
  AudioMemory(10);
  sgtl5000_1.enable();
  sgtl5000_1.volume(0.1);

  delay(2000);
  noise1.amplitude(1.0);
}




void loop() {
  ;
}


