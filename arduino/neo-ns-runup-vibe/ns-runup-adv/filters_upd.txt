static const short highNF10 = {
 1,
 -0.0529,
 -0.8272,
 -0.5428,
 -0.1440
};

static const short highNF11 = {
 1,
 0.3678,
 0.5761,
 0.2854,
 0.1290
};

static const short highNF12 = {
 1,
 -1.4655,
 0.8672,
 -1.3391,
 0.6905
};

static const short highNF13 = {
 1,
 1.6317,
 0.8337,
 1.6181,
 0.8098
};

static const short highNF20  = {
 1,
 -1.6379,
 0.7627,
 -1.7834,
 0.8293
};

static const short highNF21  = {
 1,
 1.2902,
 0.8866,
 1.2335,
 0.8601
};

static const short highNF22  = {
 1,
 0.0913,
 0.7647,
 0.1353,
 0.8611
};

static const short highNF23  = {
 1,
 -0.8016,
 0.8430,
 -0.8615,
 0.8877
};

static const short lowNF10 = {
 1,
 -0.1420,
 -0.7262,
 -0.5867,
 0.2833
};

static const short lowNF11 = {
 1,
 -0.3778,
 0.7679,
 -1.6011,
 0.6581
};

static const short lowNF12 = {
 1,
 -1.4712,
 0.8657,
 -1.2276,
 0.7673
};

static const short lowNF13 = {
 1,
 1.2655,
 0.8583,
 1.1605,
 0.7933
};

static const short lowNF20= {
 1,
 -1.7610,
 0.8832,
 -1.7324,
 0.7957
};

static const short lowNF21= {
 1,
 0.4125,
 0.8438,
 0.3275,
 0.8012
};

static const short lowNF22= {
 1,
 1.5082,
 0.7742,
 1.5671,
 0.8130
};

static const short lowNF23= {
 1,
 -0.9776,
 0.8789,
 -0.9304,
 0.8827
};

static const short TRF10= {
 1,
 1.8073,
 0.8219,
 0.2285,
 -0.5074
};

static const short TRF11= {
 1,
 -1.8470,
 0.8769,
 -0.0087,
 -0.8976
};

static const short TRF12= {
 1,
 1.2809,
 0.6878,
 1.1034,
 0.5850
};

static const short TRF13 = {
 1,
 -1.0499,
 0.7802,
 -1.0950,
 0.7894
};

static const short TRF20= {
 1,
 -1.4407,
 0.8016,
 -14769,
 0.8103
};

static const short TRF21= {
 1,
 -0.4517,
 0.8230,
 -0.4397,
 0.8238
};

static const short TRF22= {
 1,
 0.1594,
 0.8218,
 0.1120,
 0.8336
};

static const short TRF23= {
 1,
 0.9766
 0.8493,
 1.0031,
 0.8736
};
