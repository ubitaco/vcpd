with open('coeff_N_high.csv') as highNFfile:
    hNF = ['{' + x[:-1] + '}' for x in highNFfile.readlines()]
highNF10 = hNF[0]
highNF11 = hNF[1]
highNF12 = hNF[2]
highNF13 = hNF[3]
highNF20 = hNF[4]
highNF21 = hNF[5]
highNF22 = hNF[6]
highNF23 = hNF[7]

with open('coeff_N_low.csv') as lowNFfile:
    lNF = ['{' + x[:-1] + '}' for x in lowNFfile.readlines()]
lowNF10 = lNF[0]
lowNF11 = lNF[1]
lowNF12 = lNF[2]
lowNF13 = lNF[3]
lowNF20 = lNF[4]
lowNF21 = lNF[5]
lowNF22 = lNF[6]
lowNF23 = lNF[7]

with open('coeff_TRF.csv') as TRFfile:
    TRF = ['{' + x[:-1] + '}' for x in TRFfile.readlines()]
TRF10 = TRF[0]
TRF11 = TRF[1]
TRF12 = TRF[2]
TRF13 = TRF[3]
TRF20 = TRF[4]
TRF21 = TRF[5]
TRF22 = TRF[6]
TRF23 = TRF[7]
TRF30 = TRF[8]
TRF31 = TRF[9]
TRF32 = TRF[10]
TRF33 = TRF[11]

with open('coeff_correction.csv') as corfile:
    Cor = ['{' + x[:-1] + '}' for x in corfile.readlines()]
Cor10 = Cor[0]
Cor11 = Cor[1]
Cor12 = Cor[2]
Cor13 = Cor[3]
Cor20 = Cor[4]
Cor21 = Cor[5]
Cor22 = Cor[6]
Cor23 = Cor[7]


with open('filter_format.txt') as filterformat:
    output = filterformat.read().format(
        highNF10,
        highNF11,
        highNF12,
        highNF13,
        highNF20,
        highNF21,
        highNF22,
        highNF23,
        lowNF10,
        lowNF11,
        lowNF12,
        lowNF13,
        lowNF20,
        lowNF21,
        lowNF22,
        lowNF23,
        TRF10,
        TRF11,
        TRF12,
        TRF13,
        TRF20,
        TRF21,
        TRF22,
        TRF23,
        TRF30,
        TRF31,
        TRF32,
        TRF33,
        Cor10,
        Cor11,
        Cor12,
        Cor13,
        Cor20,
        Cor21,
        Cor22,
        Cor23
    )

with open('filter2.h', 'w') as filters:
    filters.write(output)
